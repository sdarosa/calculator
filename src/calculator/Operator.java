package calculator;

public enum Operator {
	PLUS("+"), MINUS("-"), MULTIPLY("*"), DIVIDE("/");
	
	String value;
	private Operator(String value) {
		this.value = value;
	}
	
	public String getOperatorValue() {
		return this.value;
	}
	
	public static Operator getFromValue(String operatorValue) {
		switch(operatorValue) {
			case "+": return Operator.PLUS;
			case "-": return Operator.MINUS;
			case "*": return Operator.MULTIPLY;
			case "/": return Operator.DIVIDE;
			default:
				return Operator.PLUS;			
		}
	}
}

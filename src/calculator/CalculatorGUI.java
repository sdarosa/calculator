package calculator;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculatorGUI {
	
	JButton btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
	JButton btnDot;
	JButton btnAddition, btnSubtraction, btnMultiplication, btnDivision;
	JButton btnEqual, btnReset;
	JLabel lblDisplay;
	JTextField txtResult;
	JPanel panelTop, panelCenter, panelEast;
	
	boolean opClicked = false;
	
	
	public void go() {
		//JPanels
		panelTop = new JPanel();
		panelCenter = new JPanel();
		panelEast = new JPanel();
		panelTop.setPreferredSize(new Dimension(260,100));
		panelEast.setPreferredSize(new Dimension(80,200));
		panelCenter.setPreferredSize(new Dimension(180,200));
		//JFrame
		JFrame frame = new JFrame();
		//Buttons
		btn0 = new JButton("0");
		btn1 = new JButton("1");
		btn2 = new JButton("2");
		btn3 = new JButton("3");
		btn4 = new JButton("4");
		btn5 = new JButton("5");
		btn6 = new JButton("6");
		btn7 = new JButton("7");
		btn8 = new JButton("8");
		btn9 = new JButton("9");
		btnDot = new JButton(".");
		btnAddition = new JButton("+");
		btnSubtraction = new JButton("-");
		btnMultiplication = new JButton("*");
		btnDivision = new JButton("/");
		btnEqual = new JButton("=");
		btnReset = new JButton("Reset");
		//label
		lblDisplay = new JLabel();
		//textfield
		txtResult = new JTextField(20);
		txtResult.setText(null);		//set default value 
		txtResult.setEditable(false);
		
		
		//add action listeners
		//reset button
		btnReset.addActionListener(new BtnResetListener());		
		//numbers
		btn0.addActionListener(new ButtonListener("0"));
		btn1.addActionListener(new ButtonListener("1"));
		btn2.addActionListener(new ButtonListener("2"));
		btn3.addActionListener(new ButtonListener("3"));
		btn4.addActionListener(new ButtonListener("4"));
		btn5.addActionListener(new ButtonListener("5"));
		btn6.addActionListener(new ButtonListener("6"));
		btn7.addActionListener(new ButtonListener("7"));
		btn8.addActionListener(new ButtonListener("8"));
		btn9.addActionListener(new ButtonListener("9"));
		btnDot.addActionListener(new ButtonListener("."));
		//Operators
		btnAddition.addActionListener(new OperatorButtonListener(Operator.PLUS));
		btnSubtraction.addActionListener(new OperatorButtonListener(Operator.MINUS));
		btnMultiplication.addActionListener(new OperatorButtonListener(Operator.MULTIPLY));
		btnDivision.addActionListener(new OperatorButtonListener(Operator.DIVIDE));
		
		//Equal
		btnEqual.addActionListener(new BtnEquListener());
		
		//add elements to panels
		//NORTH PANEL
		panelTop.add(lblDisplay);
		panelTop.add(txtResult);
		panelTop.add(btnReset);
		//CENTER PANEL
		panelCenter.add(btn7);
		panelCenter.add(btn8);
		panelCenter.add(btn9);
		panelCenter.add(btn4);
		panelCenter.add(btn5);
		panelCenter.add(btn6);
		panelCenter.add(btn1);
		panelCenter.add(btn2);
		panelCenter.add(btn3);
		panelCenter.add(btn0);
		panelCenter.add(btnDot);
		panelCenter.add(btnEqual);
		
		//EAST PANEL
		panelEast.add(btnAddition);
		panelEast.add(btnSubtraction);
		panelEast.add(btnMultiplication);
		panelEast.add(btnDivision);
		
		frame.getContentPane().add(BorderLayout.NORTH, panelTop);
		frame.getContentPane().add(BorderLayout.CENTER, panelCenter);
		frame.getContentPane().add(BorderLayout.EAST, panelEast);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(260,300);
		frame.setVisible(true);
	}
	
	//method that will do the operation and set the boolean values accordingly
	public void doCalculation(String[] tokens) {
		if(tokens.length > 2) {
			
			BigDecimal num1, num2;		
			String op;
			int counter = 0, i = 0, j = 0;
			
			num1 = new BigDecimal(tokens[counter]);
			
			while(counter < (tokens.length - 1)) {			
				j = counter + 1;
				i = counter + 2;
				op = tokens[j];
				num2 = new BigDecimal(tokens[i]);
				
				num1 = getResult(num1, num2, Operator.getFromValue(op));				
				counter += 2;				
			}
			System.out.println("result is " + num1);
			txtResult.setText("" + num1);
			lblDisplay.setText(null);
		}
	}
	//returns the result of the "num1 opType num2" operation as an integer (ex. 1 + 2).
	public BigDecimal getResult(BigDecimal num1, BigDecimal num2, Operator operator) {		
		
		switch(operator) {
			case DIVIDE: return num1.divide(num2,RoundingMode.HALF_UP);
			case MINUS: return num1.subtract(num2);
			case MULTIPLY: return num1.multiply(num2);
			case PLUS: return num1.add(num2);
		}
		return BigDecimal.ZERO;
	}
	
	//Listener for the buttons
	//Reset
	class BtnResetListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			txtResult.setText(null);
			lblDisplay.setText(null);
		}
	}	
	
	class ButtonListener implements ActionListener {
		String buttonValue;		
		public ButtonListener(String value) {
			buttonValue = value;
		}		
		public void actionPerformed(ActionEvent e) {
			String temp = buttonValue;
			if(txtResult.getText() != null && opClicked == false) {
				temp = txtResult.getText() + buttonValue;
			}
			txtResult.setText(temp);
			opClicked = false;
		}
	}
	
	class OperatorButtonListener extends ButtonListener {
		public OperatorButtonListener(Operator value) {
			super(" " + value.getOperatorValue() + " ");
		}
		
		public void actionPerformed(ActionEvent e) {
			String temp = txtResult.getText() + buttonValue;
			if(lblDisplay.getText() != null) {
				lblDisplay.setText(lblDisplay.getText() + temp);
			} else {
				lblDisplay.setText(temp);
			}
			opClicked = true;
		}
	}	
	
	//Equal
	class BtnEquListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String display = lblDisplay.getText() + txtResult.getText();			
			String[] tokens = display.split(" ");				
			doCalculation(tokens);			
		}
	}
	

}
